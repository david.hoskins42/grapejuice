title: Install Wine
---
## About the mouse bug

When locking the cursor (not allowing the cursor to move) like by adjusting the camera with right click,
the cursor does not get unlocked.

To fix this, a patched version of Wine is required.

## Installing a patched version of Wine

**Warning:** You should decide if you trust the authors of the below resources before using them.

There are two options available:

- Use a precompiled patched Wine build by running [this Python script](https://pastebin.com/raw/5SeVb005).
  The script requires Python 3.8 or above.
- Compile Wine yourself using [this guide](https://github.com/e666666/robloxWineBuildGuide).

## Installing vanilla Wine

Vanilla Wine does not currently have the mouse patch, meaning the mouse bug will occur when using vanilla Wine.

If your distribution provides the `wine` package, install that from your package manager.

Otherwise, use the [download page for Wine](https://wiki.winehq.org/Download).
