title: Install Grapejuice on Arch Linux and similar distributions
---
## Preamble

:question: If you didn't click on the guide for Arch Linux, but ended up on this page regardless, please do not panic!
Arch Linux is a distribution that is the base for other distributions in the Linux landscape. This guide is applicable
to the following distributions:

- Arch Linux
- Manjaro Linux

---

:computer: Grapejuice assumes your desktop is configured properly. Even though Arch Linux to some, is all about
minimalism, it is recommended that you run your desktop session using a display manager.

---

:package: This setup guide assumes you have AUR support enabled on your system, which implies that the `base-devel`
package is installed and that your account can use the `sudo` command.

## Enabling 32-bit support

Even though Roblox Studio runs in 64-bit mode, 32-bit libraries are still required for some parts of the program. This
is due to backwards compatibility in the Windows operating system.

You enable 32-bit support by editing `/etc/pacman.conf` with your favourite editor, where you uncomment the multilib
repository. Note that you have to be root in order to edit the file. The resulting file should contain the following:

```ini
[multilib]
Include = /etc/pacman.d/mirrorlist
```

## Synchronize the package database

Before installing anything, you should always synchronize the package database in order to prevent strange
package-not-found errors.

```sh
sudo pacman -Syu
```

## Installing Wine

It's recommended that you install a patched version of Wine. See [this guide](../Guides/Installing-Wine)
for more information.

## Installing dependencies for audio

In order for sound to work, you need to install some additional packages for the sound system you're using.

If you're not sure what sound system you're using, run `pacman -Q pulseaudio`. If the PulseAudio package is
found, PulseAudio is being used.

Otherwise, run `pacman -Q pipewire`. If the Pipewire package is found, Pipewire is being used.

If neither PulseAudio or PipeWire is installed, install one of them first.

Use the below table to find what packages to install.

| Sound system | Required Package |
|--------------|------------------|
| PulseAudio   | `lib32-libpulse` |
| PipeWire     | `pipewire-pulse` |

## Installing dependencies for networking

Install `gnutls` and `lib32-gnutls` with the following command:

```sh
sudo pacman -S gnutls lib32-gnutls
```

## Installing Grapejuice

First, you have to acquire a copy of the source code. This is easily done by cloning the git repository.

```sh
git clone https://gitlab.com/brinkervii/grapejuice.git /tmp/grapejuice
```

After the git clone command is finished, Grapejuice can be installed.

```sh
cd /tmp/grapejuice
./install.py
```
